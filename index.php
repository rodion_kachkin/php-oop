<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

// @phpcs:disable Magento2.Security.IncludeFile.FoundIncludeFile
include __DIR__ . '/Queue.php';

use Vaimo\Queue;

$items = [1, 4, 5, 7, 6, 5, 23, 65];
$queue = new Queue($items);

do {
    $item = $queue->dequeue();
    if ($item % 2 != 0) {
        $item++;
        if (!$queue->contains($item)) {
            $queue->enqueue($item);
        }
        continue;
    }
    // @phpcs:disable Magento2.Security.LanguageConstruct.DirectOutput
    echo $item, \PHP_EOL;
} while ($queue->peek());
