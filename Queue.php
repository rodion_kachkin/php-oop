<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo;

/**
 * Queue collection
 */
class Queue
{
    /**
     * @var array
     */
    private array $queue;

    /**
     * @param array $queue
     */
    public function __construct(
        array $queue = []
    ) {
        $this->queue = $queue;
    }

    /**
     * Clears all items in queue
     *
     * @return void
     */
    public function clear(): void
    {
        $this->queue = [];
    }

    /**
     * Check if queue contains item
     *
     * @param mixed $item
     * @return bool
     */
    public function contains($item): bool
    {
        foreach ($this->queue as $queueItem) {
            if ($queueItem === $item) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets front item in queue and removes from queue
     *
     * @return mixed
     */
    public function dequeue()
    {
        return \array_shift($this->queue);
    }

    /**
     * Add item to back of queue
     *
     * @param mixed $item
     * @return void
     */
    public function enqueue($item): void
    {
        $this->queue[] = $item;
    }

    /**
     * Gets front item in queue without removing it from queue
     *
     * @return mixed
     */
    public function peek()
    {
        return \current($this->queue);
    }
}
